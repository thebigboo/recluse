class Poeira {
  int anim=0;
  float x=-200000, y=-200000;
  void cria(float X, float Y) {
    x=X;
    y=Y;
    anim=0;
  }
  void desenha() {
    fill(255, 0, 0);
    if (anim<6) image (dust[anim/2], x, y);
    anim++;
    if (anim>=6) x=-2000000;
  }
}

class BrickBreak {
  int anim=0;
  float x=-200000, y=-200000;
  void cria(float X, float Y) {
    x=X-3;
    y=Y;
    anim=0;
  }
  void desenha() {
    fill(255, 0, 0);
    if (anim<6) image (brickBreak[anim/2], x, y);
    if (pause==false) {
      anim++;
      if (anim>=6) x=-2000000;
    }
  }
}

class Purpurina {
  Grao [] graos = new Grao [8];
  int opacity=255;
  Purpurina() {
    for (int i=0; i< graos.length; i++) {
      graos[i]=new Grao();
    }
  }

  void cria(float X, float Y) {
    for (int i=0; i< graos.length; i++) {
      graos[i].cria(X, Y);
    }
    opacity=400;
  }
  void desenha() {
    fill(0, 255, 255, opacity);
    stroke(0, 255, 255, opacity);
    for (int i=0; i< graos.length; i++) {
      if (opacity>0) graos[i].desenha();
    }
    noStroke();
  }
  void atualiza() {
    for (int i=0; i< graos.length; i++) {
      if (opacity>0) graos[i].atualiza();
    }
    opacity-=15;
  }
}

class Grao {
  float x=-3000;
  float y;
  float vx;
  float vy;
  void cria(float X, float Y) {
    x=X;
    y=Y;
    vx= random (-3, 3);
    vy= random (-6, 0);
  }
  void desenha() {
    //point(x, y);
    rect (x, y, 1, 1);
  }
  void atualiza() {
    x+=vx;
    y+=vy;
    vy+=gNorm;
  }
}
