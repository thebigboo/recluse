import ddf.minim.*;
//import processing.opengl.*; NEVER AGAIN!

Minim minim;
Sons SE;
Musica BGM;

int tmxNumber=1;

int w=64, h=37;
float originX, originY;
float screenW=160, screenH=128;
float screenvx=0, screenvy=0;
boolean crescedireita, crescebaixo;
int tsize=16;

int state = -1;
byte currentStage =0;
byte phase=0;
int tsw=16, sol=8;
byte frames;
int timer=0;
int menu=0;
boolean freeze=false;
boolean pause=false;
boolean canPause=true;
boolean showText=false;
int currentItem=0;

Input input;
Stage stage;
Check check;
Poeira poeira;
BrickBreak brickB [] = new BrickBreak [2];
Purpurina purpur;
byte sprite[] = new byte [64*38];
Objeto objeto[] = new Objeto[80];

PImage splash;
PImage tileset;
PImage tela;
PImage imgterreno [] = new PImage [64];
PImage telinha [] = new PImage [2];
PImage stack [] = new PImage [2];
PImage lesma[] = new PImage [8];
PImage acaro[] = new PImage [2];
PImage dust[] = new PImage [3];
PImage brickBreak[] = new PImage [3];
PImage mola[] = new PImage [3];
PImage item[] = new PImage [4];
PImage figure[] = new PImage [4];
PImage instruction[] = new PImage [4];
PImage heart[] = new PImage [2];
PImage floppy;
PImage window;
PImage gameover;
PImage title[] = new PImage [2];
PImage ending;
PImage shell[] = new PImage [2];
PImage bubble;
PFont font;

float gNorm= 0.35;
int darkness=128;
int fade = 255;
int fadeColor = color(255);
String fadeState = "in";
int fade2;
int doubleTapL=0, doubleTapR=0;
byte bumpT;

Heroi hero;

boolean itemGolpeBaixo = false, itemPuloDuplo = false, itemDash=false, secret1=false, secret2=false, secret3=false, secret4=false;
boolean h1=false, h2=false, h3=false, h4=false;
int secrets=0;
int itens=0;
int musicState=0;

public void init() {
  frame.removeNotify(); 
  //frame.setUndecorated(true);   
  frame.addNotify(); 
  super.init();
}

void setup() {
  setBackground(new java.awt.Color(12, 0, 75));
  System.setProperty("sun.awt.noerasebackground", "true");
  frameRate(28);//28
  size (160, 128);//(int(screenW), int(screenH)); //1024, 600
  frame.setResizable(true);

  noStroke();
  stage= new Stage(this);
  input= new Input();
  check=new Check();
  poeira=new Poeira();
  brickB[0] = new BrickBreak();
  brickB[1] = new BrickBreak();
  purpur=new Purpurina();
  //liquid=new Liquid();
  hero= new Heroi();
  splash = loadImage("splash.png");
  for (int i = 0; i< lesma.length; i++) lesma[i] = loadImage("lesma"+i+".gif");
  for (int i = 0; i< telinha.length; i++) telinha[i] = loadImage("minititle"+i+".gif");
  for (int i = 0; i< stack.length; i++) stack[i] = loadImage("stack"+i+".gif");
  for (int i = 0; i< mola.length; i++) mola[i] = loadImage("mola"+i+".gif");
  for (int i = 0; i< dust.length; i++) dust[i] = loadImage("poeira"+i+".gif");
  for (int i = 0; i< brickBreak.length; i++) brickBreak[i] = loadImage("break"+i+".gif");
  for (int i = 0; i< acaro.length; i++) acaro[i] = loadImage("acaro"+i+".gif");
  for (int i = 0; i< item.length; i++) item[i] = loadImage("item"+i+".gif");
  for (int i = 0; i< figure.length; i++) figure[i] = loadImage("figure"+i+".gif");
  for (int i = 0; i< heart.length; i++) heart[i] = loadImage("heart"+i+".gif");
  for (int i = 0; i< instruction.length; i++) instruction[i] = loadImage("instructions"+i+".gif");
  floppy = loadImage("floppy.gif");
  gameover = loadImage("gameOver.gif");
  for (int i = 0; i< title.length;i++) title[i] = loadImage("title"+i+".gif");
  for (int i = 0; i< shell.length; i++) shell[i] = loadImage("shell"+i+".gif");
  bubble=loadImage("bubble0.gif");
  font = loadFont("font.vlw");

  SE = new Sons();
  BGM = new Musica();
  minim=new Minim (this);
  SE.initAllSounds();
  BGM.initAllTracks();

  originX=24*tsize;
  originY=14*tsize;
  textFont(font);
}



void draw() {
  switch(state) {
  case 0:
    drawTitle();
    break;

  case 2:
    drawGameOver();
    break;

  case 3:
    drawGame();
    break;

  case 4:
    drawEnding();
    break;
    
  case -1:
    drawSplash();
    break;

  default:
    break;
  }
  if (pause==false) {

    frames++;
    if (frames>=60) {
      frames=0;
      timer++;
    }
    if (freeze==true && showText==true && timer==2) {
      freeze=false;
      showText=false;
    }
    if (freeze==true && showText==true && input.start==true) {
      freeze=false;
      showText=false;
    }
    if (doubleTapL > 0)doubleTapL--;
    if (doubleTapR > 0)doubleTapR--;

    screenW+=screenvx;
    screenH+=screenvy;
    if (crescedireita==false) originX-=screenvx;
    if (crescebaixo==false) originY -=screenvy;
    if (screenvx<0) screenvx=0;
    if (screenvy<0) screenvy=0;
    //frame.setLocation(-30+screen.width/2-80+int(originX)-24*tsize, -60+screen.height/2-64+int(originY)-14*tsize);
    //frame.setSize(int(screenW)+frame.getInsets().left+frame.getInsets().right, int(screenH)+frame.getInsets().top+frame.getInsets().bottom);
     javax.swing.SwingUtilities.invokeLater(new Runnable() {
    public void run() {
      frame.setLocation(-30+displayWidth/2-80+int(originX)-24*tsize, -60+displayHeight/2-64+int(originY)-14*tsize);
      frame.setSize(int(screenW)+frame.getInsets().left+frame.getInsets().right, int(screenH)+frame.getInsets().top+frame.getInsets().bottom);
    }
  });
  }
}

void drawTitle() {
  rectMode (CORNER); 
  imageMode(CORNER);
  image(title[frames%2], 0, 0);
  fill(fadeColor, fade);
  rect(0, 0, width, height);
  
  if (fadeColor == color(255)) {
    fade -= 10;
    if (fade <= 0) {
      fade = 0;
      fadeColor = color(12, 0, 75);
    }
  }
  else {
    
    if (fade<0) fade++;
    if (keyPressed==true && fade==0) {
      tmxNumber=1;
  
      fade=1;    
      BGM.startBGM();
    }
    if (fade>0) {
      if (input.key1==true && input.key2==true && input.key3==true) tmxNumber=2;
      fade+=8;
    }
    if (fade>300) {  
      stage.loadTiles(tmxNumber+".json");
      stage.loadObjects(tmxNumber+".json");
      tileset=loadImage("tileset"+tmxNumber+".png");
      for (int i = 0; i<16; i++) for (int j = 0; j< 4; j++) {
        imgterreno[i+j*16]=tileset.get(i*16, j*16, 16, 16);
      }
      state=3;
      hero.start();
    }
  }
}

void drawGame() {
  translate(int(-originX), int(-originY));
  if (bumpT>0 && pause==false) bumpT--;
  translate(0, sin(bumpT));
  background(0);

  if (tmxNumber==1) {
    if (musicState==0 && screenvx>0 && crescedireita==true) {
      musicState++;
      BGM.nextTrack(musicState);
    }
    if (musicState==1 && screenvy>0 && crescebaixo==true) {
      musicState++;
      BGM.nextTrack(musicState);
    }
    if (musicState==2 && screenvx>0 && crescedireita==true) {
      musicState++;
      BGM.nextTrack(musicState);
    }
    if (musicState==3 && screenvx>0 && crescedireita==false && hero.y<113) {
      musicState++;
      BGM.nextTrack(musicState);
    }
  }
  else {
    if (musicState==0 && screenvx>0 && crescedireita==false) {
      musicState++;
      BGM.nextTrack(musicState);
    }
    if (musicState==1 && screenvy>0 && crescebaixo==true) {
      musicState++;
      BGM.nextTrack(musicState);
    }
    if (musicState==2 && screenvy>0 && crescebaixo==false) {
      musicState++;
      BGM.nextTrack(musicState);
    }
    if (musicState==3 && screenvx>0 && crescedireita==true && hero.y<113) {
      musicState++;
      BGM.nextTrack(musicState);
    }
  }

  if (pause==false) hero.atualiza();

  for (int i = 0; i<w;i++) for (int j = 0; j<h;j++) {
    if (sprite[i+w*j]%tsw<=7)image(imgterreno[sprite[i+w*j]], i*16, j*16);
  }
  if (pause==false)purpur.atualiza();
  purpur.desenha();
  for (int i=0; i< objeto.length; i++) {
    if (pause==false)objeto[i].atualiza();
    objeto[i].desenha();
  }
  image(stack[1], 24*tsize, 14*tsize);
  image (telinha[frames%2], 24*tsize+28, 14*tsize+92);
  if (secret1==true) image(figure[1], 25*tsize-1, 16*tsize-6);
  if (secret2==true) image(figure[0], 26*tsize-1, 16*tsize-6);
  if (secret3==true) image(figure[2], 31*tsize-1, 16*tsize-6);
  if (secret4==true) image(figure[3], 32*tsize-1, 16*tsize-6);
  if (hero.flinch<=0 || frames%2==0)hero.desenha();
  image(stack[0], 24*tsize, 14*tsize);
  poeira.desenha();
  brickB[0].desenha();
  brickB[1].desenha();
  for (int i = 0; i<w;i++) for (int j = 0; j<h;j++) {
    if (sprite[i+w*j]%tsw>7)image(imgterreno[sprite[i+w*j]], i*16, j*16);
  }
  if (pause==false) {
    if (screenvx>0) screenvx-=0.04;
    if (screenvy>0) screenvy-=0.04;
  }
  if (darkness>0) {
    fill(12, 0, 75, darkness);
    rect (originX, originY, width, height);
    if (pause ==false && screenW>160)darkness--;
  }
  for (int i=0; i<hero.lifemax; i++) image (heart[0], originX+8+12*i, originY+8);
  for (int i=0; i<hero.life; i++) image (heart[1], originX+8+12*i, originY+8);
  if (fade>0) {
    fill(12, 0, 75, fade);
    rect (originX, originY, width, height);
    if (pause == false) fade-=8;
  }
  if (fade2>300) {
    BGM.ending();
    ending=loadImage("ending.gif");
    screenvx=0;
    hero.v=0;
    hero.walkspeed=0;
    hero.x=width/2;
    hero.y=height/2-20;
    pause=false;
    timer=0;
    frames=0;
    state=4;
    if (secrets>=4)bubble=loadImage("bubble1.gif");
    if (tmxNumber==2 && secrets==4)for (int i = 0; i< lesma.length; i++) lesma[i] = loadImage("lesmas"+i+".gif");
  }
  if (showText==true) {
    imageMode(CENTER);
    rectMode(CENTER);
    fill(0, 200);
    rect (originX+width/2, originY+height/2, 442, 156);
    image (window, originX+width/2, originY+height/2);
    imageMode(CORNER);
    rectMode(CORNER);
  }
  if (pause==true && fade2<=0) {
    fill(0, 200);
    rect(originX, originY, width, height);
    imageMode (CENTER);
    image(instruction[itens], originX+width/2, originY+16);
    imageMode (CORNER);
    fill(255, 0, 0);
    textAlign(CENTER);
    textFont(font);
    text("GAME PAUSED", originX+width/2, originY+height/2-12);
    fill(255);
    textAlign(LEFT);
    text("RESUME", originX+width/2-24, originY+height/2+8);
    text("TITLE", originX+width/2-24, originY+height/2+24);
    text("QUIT", originX+width/2-24, originY+height/2+40);
    text(">", originX+width/2-32, originY+height/2+8+16*menu);
    textAlign(RIGHT);
    if (secrets>0)text("Collectibles: "+secrets+"/4", originX+screenW-8, originY+screenH-8);
  }
  if (fade2>0) {
    fill(255, fade2);
    rect(originX, originY, width, height);
    fade2+=8;
  }
  if (tmxNumber==1 && hero.x>(w-1)*tsize-6 && fade2<=0) {
    canPause=false;
    pause=true;
    fade2=1;
  }
  if (tmxNumber==2 && hero.x<=8 && fade2<=0) {
    canPause=false;
    pause=true;
    fade2=1;
  }
}

void drawSplash(){
  background(255);
  imageMode(CENTER);
  image(splash, width/2, height/2);
  fill(fadeColor, fade);
  rect(0, 0, width, height);
  if (frameCount >70){
    fade += 10;
    if (fade >= 255) {
      state = 0;
    }
  }
  else if (fade > 0){
    fade -= 10;
  }
}

void bump() {
  SE.sBump();
  bumpT=8;
}

void bump2() {
  SE.sBump();
  bumpT=28;
  if (freeze==true && showText==true && currentItem==0) {
    freeze=false;
    showText=false;
  }
}

void imageY(PImage I, float X, float Y) {
  pushMatrix();
  translate(0, Y+I.height);
  scale(1, -1);
  image (I, X, 0);
  popMatrix();
}

void imageX(PImage I, float X, float Y) {
  pushMatrix();
  translate(X+I.width, 0);
  scale(-1, 1);
  image (I, 0, Y);
  popMatrix();
}

void imageXC(PImage I, float X, float Y) {
  pushMatrix();
  translate(X, 0);
  scale(-1, 1);
  image (I, 0, Y);
  popMatrix();
}

void RImage(PImage I, float X, float Y, float A) {
  pushMatrix();
  translate (X, Y);
  rotate(A);
  image(I, 0, 0);
  popMatrix();
}

void gameOver() {
  screenvx=0;
  screenvy=0;
  BGM.fadeOutAll();
  state=2;
  timer=0;
  bumpT=0;
}
void restart() {
  state=3;
  check.start();
}

void backToTitle() {
  fade=-10;
  restart();
  state=0;
  BGM.fadeOutAll();
}

void openWindow(int I) {
  if (I<3) window=loadImage("window"+I+".gif");
  else window=loadImage("window3.gif");
  currentItem=I;
  freeze=true;
  showText=true;
  timer=0;
}

void drawGameOver() {
  fill(0, 20);
  if (timer>=1)rect (0, 0, width, height);
  if (timer>=2) {
    imageMode(CENTER);
    image (gameover, width/2, height/2);
    imageMode(CORNER);
    if (keyPressed==true) {
      if (check.usedCheckpoint==false) restart();
      else {
        state=3;
        check.load();
      }
    }
  }
}

void drawEnding() {
  background(0);
  imageMode(CENTER);
  rectMode (CORNER);
  image (ending, width/2, height/2);
  if (tmxNumber==1)image (shell[0], width/2, height/2-40);
  else {
    if (secrets<4) imageXC (shell[0], width/2, height/2-40);
    else{
    if (timer*30+frames<30)imageXC (shell[0], width/2, height/2-40);
    else if (timer*30+frames<50)image(shell[1], width/2+random(-4,4), height/2-40+random(-4,4));
    }
  }
  

  if (timer>0 && frames<=30 && timer<5) imageX (lesma[(frames/6)%2], hero.x-lesma[0].width, hero.y);
  else image (lesma[(frames/6)%2], hero.x, hero.y);

  fill(0);
  rect(0, 0, width/2-80-hero.v, height);
  rect(0, 0, width, height/2-64-hero.walkspeed);
  rect(width/2+80+hero.v, 0, width, height);
  rect(0, height/2+64+hero.walkspeed, width, height);
  if (fade2>0) {
    fill(255, fade2);
    rect (0, 0, width, height);
    fade2-=8;
  }
  if (timer<1)hero.y++;
  if (timer>=2)hero.v+=2;
  if (hero.v>169)hero.walkspeed+=2;
  rectMode (CENTER); 
  fill(255);
  if (timer*60+frames>=400) image (bubble, width/2, 239);
  if (timer*60+frames>=400 && timer*60+frames<=470)rect (width/2, 185+18*1, 390, 13);
  if (timer*60+frames>=400 && timer*60+frames<=550)rect (width/2, 185+18*2, 390, 13);
  if (timer*60+frames>=400 && timer*60+frames<=590)rect (width/2, 185+18*3, 390, 13);


  fill(0, 200);
  if (timer*60+frames==700) BGM.nextTrack(1);  
  if (timer*60+frames>=700) rect (width/2, 464, 670, 176);
  fill(255);
  textAlign (CENTER);
  if (timer*60+frames>=700) text ("-FIN-", width/2, 406+32*0);
  if (timer*60+frames>=750) text ("You found "+secrets+" of 4 collectibles during your adventure!", width/2, 406+32*1);
  if (timer*60+frames>=800) {
    if (secrets>=4) text ("As a reward, let me tell you a secret:", width/2, 406+32*2);
    else text ("Get all 4 to find out about another game mode!", width/2, 406+32*2);
  }
  if (timer*60+frames>=850) {
    if (tmxNumber==1) {
      if (secrets>=4) text ("Press the \"A\", \"N\" and \"Y\" keys at the same time during the title screen.", width/2, 406+32*3);
      else text ("Thanks for playing!", width/2, 406+32*3);
    }
    else {
      if (secrets>=4) text ("Press any key during the title screen to play on normal mode!", width/2, 406+32*3);
      else text ("Thanks for playing!", width/2, 406+32*3);
    }
  }
  if (timer*60+frames>=900 && (((timer*60+frames)/15)&1)==0) {
    text ("(Press ENTER to go back to title)", width/2, 406+32*4);
  }
}
