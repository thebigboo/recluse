class Mola extends Objeto {
  byte anim=0;
  Mola(int X, int Y) {
    x= X*tsize;
    y=Y*tsize+16;
    sizex=16;
    sizey=6;
  }
  void desenha() {
    if (anim==0) image(mola[0], x, y-16);
    else if (anim%4<3) image(mola[anim%4], x, y-16);
    else image(mola[2], x, y-16);
  }
  void atualiza() {
    if (anim>0) anim--;
    if (hero.x+hero.sizex>x && hero.x<x+sizex && hero.y>y-sizey && hero.y<y+sizey) {
      SE.sSpring();
      hero.y=y;
      hero.jump();
      hero.canDoubleJump=true;
      hero.v=-90*0.1;
      anim=16;
      hero.golpeCima=true;
      hero.golpeBaixo=false;
    }
  }
}

class Heart1 extends Objeto {
  Heart1(int X, int Y) {
    x= X*tsize+16;
    y=Y*tsize+8;
    if (h1==true) {
      x=-10000;
      y=-10000;
    }
  }
  void desenha() {
    imageMode (CENTER);
    image (heart[1], x, y+(frames/30)%2);
    imageMode (CORNER);
  }
  void atualiza() {
    if (x>hero.x && x< hero.x+hero.sizex && y>hero.y-hero.sizey && y< hero.y && hero.flinch<=0) {
      SE.sHeart();
      hero.life++;
      hero.lifemax++;
      x=-10000;
      y=-10000;
      h1=true;
    }
  }
}

class Heart2 extends Objeto {
  Heart2(int X, int Y) {
    x= X*tsize+16;
    y=Y*tsize+8;
    if (h2==true) {
      x=-10000;
      y=-10000;
    }
  }
  void desenha() {
    imageMode (CENTER);
    image (heart[1], x, y+(frames/30)%2);
    imageMode (CORNER);
  }
  void atualiza() {
    if (x>hero.x && x< hero.x+hero.sizex && y>hero.y-hero.sizey && y< hero.y && hero.flinch<=0) {
      SE.sHeart();
      hero.life++;
      hero.lifemax++;
      x=-10000;
      y=-10000;
      h2=true;
    }
  }
}

class Heart3 extends Objeto {
  Heart3(int X, int Y) {
    x= X*tsize+16;
    y=Y*tsize+8;
    if (h3==true) {
      x=-10000;
      y=-10000;
    }
  }
  void desenha() {
    imageMode (CENTER);
    image (heart[1], x, y+(frames/30)%2);
    imageMode (CORNER);
  }
  void atualiza() {
    if (x>hero.x && x< hero.x+hero.sizex && y>hero.y-hero.sizey && y< hero.y && hero.flinch<=0) {
      SE.sHeart();
      hero.life++;
      hero.lifemax++;
      x=-10000;
      y=-10000;
      h3=true;
    }
  }
}

class Heart4 extends Objeto {
  Heart4(int X, int Y) {
    x= X*tsize+16;
    y=Y*tsize+8;
    if (h4==true) {
      x=-10000;
      y=-10000;
    }
  }
  void desenha() {
    imageMode (CENTER);
    image (heart[1], x, y+(frames/30)%2);
    imageMode (CORNER);
  }
  void atualiza() {
    if (x>hero.x && x< hero.x+hero.sizex && y>hero.y-hero.sizey && y< hero.y && hero.flinch<=0) {
      SE.sHeart();
      hero.life++;
      hero.lifemax++;
      x=-10000;
      y=-10000;
      h4=true;
    }
  }
}

class Fantasma extends Objeto {
  boolean direita;
  int anim;
  int xMin;
  int xMax;
  int yIn;
  int h;
  float rand;
  Fantasma(float X, float Y, int N) {
    x=X*tsize;
    y=Y*tsize;
    yIn=int(y);
    sizex=16;
    sizey=16;
    xMin=int(x);
    xMax=int(x)+N*tsize;
    rand=random(1, 100);
    for (int i = 0; i<rand;i++) {
      h++;
      if (h*0.1>TWO_PI)h-=TWO_PI*10;
      y+=sin(h*0.1);

      if (direita==true) x++;
      else x--;
      if (x>xMax)direita=false;
      if (x<xMin)direita=true;
    }
  }
  void atualiza() {
    h++;
    if (h*0.1>TWO_PI)h-=TWO_PI*10;
    y+=sin(h*0.1);

    if (direita==true) x++;
    else x--;
    if (x>xMax)direita=false;
    if (x<xMin)direita=true;
    if (x>hero.x && x< hero.x+hero.sizex && y>hero.y-hero.sizey && y< hero.y && hero.flinch<=0) {
      hero.levaDano(1);
    }
  }
  void desenha() {
    imageMode (CENTER);
    pushMatrix();
    translate(x, y);
    if (direita==true)rotate(atan2(-1, sin(h*0.1))+PI);
    else rotate(atan2(1, sin(h*0.1))+PI);
    image (acaro[(frames/2)%2], 0, 0);
    popMatrix();
    imageMode (CORNER);
  }
}

class FantasmaV extends Objeto {
  boolean baixo;
  int anim;
  int yMin;
  int yMax;
  int xIn;
  int h;
  float rand;
  FantasmaV(float X, float Y, int N) {
    x=X*tsize;
    y=Y*tsize;
    xIn=int(x);
    sizex=16;
    sizey=16;
    yMin=int(y);
    yMax=int(y)+N*tsize;
    rand=random(1, 100);
    for (int i = 0; i<rand;i++) {
      h++;
      if (h*0.1>TWO_PI)h-=TWO_PI*10;
      x+=sin(h*0.1);

      if (baixo==true) y++;
      else y--;
      if (y>yMax)baixo=false;
      if (y<yMin)baixo=true;
    }
  }
  void atualiza() {
    h++;
    if (h*0.1>TWO_PI)h-=TWO_PI*10;
    x+=sin(h*0.1);

    if (baixo==true) y++;
    else y--;
    if (y>yMax)baixo=false;
    if (y<yMin)baixo=true;
    if (x>hero.x && x< hero.x+hero.sizex && y>hero.y-hero.sizey && y< hero.y && hero.flinch<=0) {
      hero.levaDano(1);
    }
  }
  void desenha() {
    imageMode (CENTER);
    pushMatrix();
    translate(x, y);
    if (baixo==true)rotate(atan2(-sin(h*0.1), 1)+PI);
    else rotate(atan2(sin(h*0.1), 1));
    image (acaro[(frames/2)%2], 0, 0);
    popMatrix();
    imageMode (CORNER);
  }
}

class Checkpoint extends Objeto {
  boolean saving=false;
  Checkpoint(float X, float Y) {
    x= X*tsize;
    y=Y*tsize+16;
    sizex=16;
    sizey=16;
  }
  void desenha() {
    if (saving==true) tint(255, 128);
    image(floppy, x, y-sizey+(frames/30)%2);
    tint(255);
  }
  void atualiza() {
    if (hero.x+hero.sizex>x && hero.x<x+sizex && hero.y>y-sizey && hero.y<y+hero.sizey) {
      if (saving==false) {
        check.save();
        purpur.cria(x+8, y-8);
      }
      hero.life=hero.lifemax;
      saving=true;
      check.usedCheckpoint=true;
    }
    else if (dist (x, y, hero.x, hero.y)>30) saving=false;
  }
}

class Item1 extends Objeto {
  Item1(int X, int Y) {
    x= X*tsize;
    y=Y*tsize+16;
    sizex=16;
    sizey=16;
    if (itemGolpeBaixo==true) {
      x=-10000;
      y=-10000;
    }
  }
  void desenha() {
    image(item[0], x, y-sizey);
  }
  void atualiza() {
    if (hero.x+hero.sizex>x && hero.x<x+sizex && hero.y>y-sizey && hero.y<y+hero.sizey) {
      SE.sItem();
      itemGolpeBaixo=true;
      itens++;
      openWindow(0);
      x=-10000;
      y=-10000;
    }
  }
}

class Item2 extends Objeto {
  Item2(int X, int Y) {
    x= X*tsize;
    y=Y*tsize+16;
    sizex=16;
    sizey=16;
    if (itemPuloDuplo==true) {
      x=-10000;
      y=-10000;
    }
  }
  void desenha() {
    image(item[1], x, y-sizey);
  }
  void atualiza() {
    if (hero.x+hero.sizex>x && hero.x<x+sizex && hero.y>y-sizey && hero.y<y+hero.sizey) {
      SE.sItem();
      itemPuloDuplo=true;
      itens++;
      openWindow(1);
      x=-10000;
      y=-10000;
    }
  }
}

class Item3 extends Objeto {
  Item3(int X, int Y) {
    x= X*tsize;
    y=Y*tsize+16;
    sizex=16;
    sizey=16;
    if (itemDash==true) {
      x=-10000;
      y=-10000;
    }
  }
  void desenha() {
    image(item[2], x, y-sizey);
  }
  void atualiza() {
    if (hero.x+hero.sizex>x && hero.x<x+sizex && hero.y>y-sizey && hero.y<y+hero.sizey) {
      SE.sItem();
      itemDash=true;
      itens++;
      openWindow(2);
      x=-10000;
      y=-10000;
    }
  }
}

class Secret1 extends Objeto {
  Secret1(float X, int Y) {
    x= X*tsize;
    y=Y*tsize+16;
    sizex=16;
    sizey=16;
    if (secret1==true) {
      x=-10000;
      y=-10000;
    }
  }
  void desenha() {
    image(item[3], x, y-sizey);
  }
  void atualiza() {
    if (hero.x+hero.sizex>x && hero.x<x+sizex && hero.y>y-sizey && hero.y<y+hero.sizey) {
      SE.sItem2();
      purpur.cria(25*tsize+7, 16*tsize-6);
      secret1=true;
      secrets++;
      openWindow(3);
      x=-10000;
      y=-10000;
    }
  }
}

class Secret2 extends Objeto {
  Secret2(float X, int Y) {
    x= X*tsize;
    y=Y*tsize+16;
    sizex=16;
    sizey=16;
    if (secret2==true) {
      x=-10000;
      y=-10000;
    }
  }
  void desenha() {
    image(item[3], x, y-sizey);
  }
  void atualiza() {
    if (hero.x+hero.sizex>x && hero.x<x+sizex && hero.y>y-sizey && hero.y<y+hero.sizey) {
      SE.sItem2();
      purpur.cria(26*tsize+7, 16*tsize-6);
      secret2=true;
      secrets++;
      openWindow(4);
      x=-10000;
      y=-10000;
    }
  }
}

class Secret3 extends Objeto {
  Secret3(float X, int Y) {
    x= X*tsize;
    y=Y*tsize+16;
    sizex=16;
    sizey=16;
    if (secret3==true) {
      x=-10000;
      y=-10000;
    }
  }
  void desenha() {
    image(item[3], x, y-sizey);
  }
  void atualiza() {
    if (hero.x+hero.sizex>x && hero.x<x+sizex && hero.y>y-sizey && hero.y<y+hero.sizey) {
      SE.sItem2();
      purpur.cria(31*tsize+7, 16*tsize-6);
      secret3=true;
      secrets++;
      openWindow(5);
      x=-10000;
      y=-10000;
    }
  }
}

class Secret4 extends Objeto {
  Secret4(float X, int Y) {
    x= X*tsize;
    y=Y*tsize+16;
    sizex=16;
    sizey=16;
    if (secret4==true) {
      x=-10000;
      y=-10000;
    }
  }
  void desenha() {
    image(item[3], x, y-sizey);
  }
  void atualiza() {
    if (hero.x+hero.sizex>x && hero.x<x+sizex && hero.y>y-sizey && hero.y<y+hero.sizey) {
      SE.sItem2();
      purpur.cria(32*tsize+7, 16*tsize-6);
      secret4=true;
      secrets++;
      openWindow(5);
      x=-10000;
      y=-10000;
    }
  }
}
