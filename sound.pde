class Sons {
  AudioSample jump;
  AudioSample spring;
  AudioSample flash;
  AudioSample sprint;

  AudioSample push;
  AudioSample bump;
  AudioSample damage;
  AudioSample quake;
  AudioSample blockBreak;
  AudioSample groundPound;

  AudioSample checkpoint;
  AudioSample heart;
  AudioSample item;
  AudioSample item2;

  AudioSample windowClose;

  //coisas relacionadas a menu tb

  void initAllSounds() {
    jump = minim.loadSample("SE/jump3.wav");
    spring = minim.loadSample("SE/spring3.wav");
    //flash = minim.loadSample("SE/flash.wav");
    sprint = minim.loadSample("SE/sprint_lower.wav");

    push = minim.loadSample("SE/push.wav");
    bump = minim.loadSample("SE/bump4_lower.wav");
    damage = minim.loadSample("SE/damage.wav");

    checkpoint = minim.loadSample("SE/checkpoint.wav");
    heart = minim.loadSample("SE/heart.wav");
    item = minim.loadSample("SE/item.wav");
    item2 = minim.loadSample("SE/item2.wav");
  }
  void sJump() {
    jump.trigger();
  }
  void sSpring() {
    bump.stop();
    spring.stop();
    spring.trigger();
  }
  void sFlash() {
    flash.trigger();
  }
  void sSprint() {
    sprint.trigger();
  }
  void sBump() {
    bump.stop();
    spring.stop();
    bump.trigger();
  }
  void sDamage() {
    damage.trigger();
  }
  void sCheckpoint() {
    checkpoint.trigger();
  }
  void sHeart() {
    heart.trigger();
  }
  void sItem() {
    item.stop();
    item.trigger();
  }
  void sItem2() {
    item2.stop();
    item2.trigger();
  }
  void sPush() {
    push.stop();
    push.trigger();
  }

  void closeAll() {
    jump.close();
    spring.close();
    flash.close();
    sprint.close();

    push.close();
    bump.close();
    damage.close();

    heart.close();
    item.close();
    item2.close();
    checkpoint.close();
  }
}




class Musica {
  AudioPlayer [] track = new AudioPlayer [5];
  int sync;
  void initAllTracks() {
    for (int i=0; i<track.length;i++) {
      track[i]= minim.loadFile("BGM/recluse"+i+".mp3");
    }
  }
  void startBGM() {
    for (int i=0; i<track.length;i++) {
      track[i].play();
      track[i].loop();
      if (i>0)track[i].shiftGain(0, -80, 0);
      else track[i].shiftGain(-80, 0, 1000);
    }
  }
  void nextTrack(int A) {
    track[A-1].shiftGain(0, -80, 500);
    track[A].shiftGain(-80, 0, 50);
  }
  void restoreTrack(int A) {
    for (int i=0; i<track.length;i++) {
      track[i].play();
      track[i].loop();
      if (i!=A)track[i].shiftGain(0, -80, 0);
      else track[i].shiftGain(-80, 0, 1000);
    }
  }
  void fadeOutAll() {
    for (int i=0; i<track.length;i++) {
      if (track[i].getGain()>-80) track[i].shiftGain(0, -80, 1000);
    }
  }
  void ending() {
    track[0].shiftGain(-80, 0, 0);
    for (int i=1; i<track.length;i++) {
      track[i].shiftGain(0, -80, 0);
    }
  }
  void closeAll() {
    for (int i=0; i<track.length;i++) {
      track[i].close();
    }
  }
}

void stop()
{
  SE.closeAll();
  BGM.closeAll();
  minim.stop();
  super.stop();
}
