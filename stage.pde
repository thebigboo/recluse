class Stage {
  PApplet p;
  Stage (PApplet parent) {
    p = parent;
  }
  
//  void load(int N) {
//    XML xml = loadXML(N+".tmx"); 
//    XML [] map = xml.getChild("layer").getChild("data").getChildren();
//    println(map[0]);
//    println(map[1]);
//    for (int i = 0; i < map.length; i++) {
//      sprite[i] = byte(map[i].getInt("gid")-1);
//      println ("sprite: "+ sprite[i]);
//    }
//    w = byte(xml.getInt("width"));
//    h = byte(xml.getInt("height"));
//  }
  
  void loadTiles(String name) {
    JSONObject json = loadJSONObject(name);
    w = byte(json.getInt("width"));
    h = byte(json.getInt("height"));
    JSONArray map = searchForJSONObject(json.getJSONArray("layers"), "name", "Camada de Tiles 1").getJSONArray("data");
    for (int i = 0; i < map.size(); i++) {
      sprite[i] = byte(map.getInt(i)-1);
    }
  }
  void loadObjects (String name) {
    for (int i = 0; i<objeto.length; i++) objeto[i]=new Mola(-10000, -10000);
    JSONObject json = loadJSONObject(name);
    JSONArray objects = searchForJSONObject(json.getJSONArray("layers"), "name", "obj").getJSONArray("objects");
    for (int i = 0; i < objects.size(); i++) {
      JSONObject o = objects.getJSONObject(i);
      String objectType = o.getString("type");
      if (objectType.equals("Mola")) objeto[i]=new Mola(o.getInt("x")/tsize, o.getInt("y")/tsize);
      if (objectType.equals("Heart1")) objeto[i]=new Heart1(o.getInt("x")/tsize, o.getInt("y")/tsize);
      if (objectType.equals("Heart2")) objeto[i]=new Heart2(o.getInt("x")/tsize, o.getInt("y")/tsize);
      if (objectType.equals("Heart3")) objeto[i]=new Heart3(o.getInt("x")/tsize, o.getInt("y")/tsize);
      if (objectType.equals("Heart4")) objeto[i]=new Heart4(o.getInt("x")/tsize, o.getInt("y")/tsize);
      if (objectType.equals("Checkpoint")) objeto[i]=new Checkpoint(o.getFloat("x")/tsize, o.getFloat("y")/tsize);

      if (objectType.equals("Fantasma4")) objeto[i]=new Fantasma(o.getFloat("x")/tsize, o.getFloat("y")/tsize, 4);
      if (objectType.equals("Fantasma5")) objeto[i]=new Fantasma(o.getFloat("x")/tsize, o.getFloat("y")/tsize, 5);
      if (objectType.equals("Fantasma6")) objeto[i]=new Fantasma(o.getFloat("x")/tsize, o.getFloat("y")/tsize, 6);
      if (objectType.equals("Fantasma8")) objeto[i]=new Fantasma(o.getFloat("x")/tsize, o.getFloat("y")/tsize, 8);
      if (objectType.equals("FantasmaV4")) objeto[i]=new FantasmaV(o.getFloat("x")/tsize, o.getFloat("y")/tsize, 4);
      if (objectType.equals("FantasmaV5")) objeto[i]=new FantasmaV(o.getFloat("x")/tsize, o.getFloat("y")/tsize, 5);
      if (objectType.equals("FantasmaV6")) objeto[i]=new FantasmaV(o.getFloat("x")/tsize, o.getFloat("y")/tsize, 6);
      if (objectType.equals("FantasmaV8")) objeto[i]=new FantasmaV(o.getFloat("x")/tsize, o.getFloat("y")/tsize, 8);

      if (objectType.equals("Item1")) objeto[i]=new Item1(o.getInt("x")/tsize, o.getInt("y")/tsize);
      if (objectType.equals("Item2")) objeto[i]=new Item2(o.getInt("x")/tsize, o.getInt("y")/tsize);
      if (objectType.equals("Item3")) objeto[i]=new Item3(o.getInt("x")/tsize, o.getInt("y")/tsize);
      if (objectType.equals("Secret1")) objeto[i]=new Secret1(o.getFloat("x")/tsize, o.getInt("y")/tsize);
      if (objectType.equals("Secret2")) objeto[i]=new Secret2(o.getFloat("x")/tsize, o.getInt("y")/tsize);
      if (objectType.equals("Secret3")) objeto[i]=new Secret3(o.getFloat("x")/tsize, o.getInt("y")/tsize);
      if (objectType.equals("Secret4")) objeto[i]=new Secret4(o.getFloat("x")/tsize, o.getInt("y")/tsize);
    }
  }
}

JSONObject searchForJSONObject (JSONArray elements, String tag, String value) {
  JSONObject o = null;
  for (int i = 0; i < elements.size(); i++){
    JSONObject currentObject = elements.getJSONObject(i);
    if (currentObject.getString(tag).equals(value)) {
      o = currentObject;
      break;
    }
  }
  return o;
}
