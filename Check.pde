class Check {
  boolean usedCheckpoint=false;
  float x;
  float y;
  int life;
  int lifemax;
  boolean temDash;
  boolean temPuloDuplo;
  boolean temGolpeBaixo;
  boolean temSecret1;
  boolean temSecret2;
  boolean temSecret3;
  boolean temSecret4;
  boolean temH1;
  boolean temH2;
  boolean temH3;
  boolean temH4;
  int quantosSecrets;
  int quantosItens;
  float checkOriginX;
  float checkOriginY;
  float checkScreenW;
  float checkScreenH;
  int checkDarkness;
  int checkMusicState;
  void start() {
    pause=false;
    hero.start();
    screenvx=0;
    screenvy=0;
    
    screenW=160;
    screenH=128; 
    originX=24*tsize;
    originY=14*tsize;
    darkness=128;
    itemGolpeBaixo = false; 
    itemPuloDuplo = false; 
    itemDash=false; 
    secret1=false; 
    secret2=false; 
    secret3=false;
    secret4=false;
    h1=false;
    h2=false;
    h3=false;
    h4=false;
    secrets=0;
    itens=0;    
    stage.loadObjects(tmxNumber+".json");
    stage.loadTiles(tmxNumber+".json");
    timer=0;
    freeze=false;
    showText=false;
    musicState=0;
    
    BGM.restoreTrack(musicState);

    frame.setLocation(-30+displayWidth/2-80+int(originX)-24*tsize, -60+displayHeight/2-64+int(originY)-14*tsize);
    frame.setSize(int(screenW)+frame.getInsets().left+frame.getInsets().right, int(screenH)+frame.getInsets().top+frame.getInsets().bottom);
    check.saveMute();
  }
  void saveMute() {
    x = hero.x;
    y=hero.y;
    life=hero.life;
    lifemax=hero.lifemax;
    temDash = itemDash;
    temPuloDuplo = itemPuloDuplo;
    temGolpeBaixo = itemGolpeBaixo;
    temSecret1 = secret1;
    temSecret2 = secret2;
    temSecret3 = secret3;
    temSecret4 = secret4;
    temH1=h1;
    temH2=h2;
    temH3=h3;
    temH4=h4;

    quantosSecrets = secrets;
    quantosItens=itens;
    checkOriginX = originX;
    checkOriginY = originY;
    checkScreenW = screenW;
    checkScreenH = screenH;
    checkDarkness = darkness;
    checkMusicState = musicState;
  }
  
  void save() {
    SE.sCheckpoint();
    saveMute();
  }
 
  void load() {
    stage.loadTiles(tmxNumber+".json");
    timer=0;
    freeze=false;
    showText=false;

    hero.x=x;
    hero.y=y;
    hero.life=life;
    hero.lifemax=lifemax;
    hero.flinch=0;
    hero.v=0;

    itemDash=temDash ;
    itemPuloDuplo =temPuloDuplo ;
    itemGolpeBaixo =temGolpeBaixo ;
    secret1 =temSecret1 ;
    secret2=  temSecret2;
    secret3=  temSecret3;
    secret4=  temSecret4;
    h1= temH1;
    h2= temH2;
    h3= temH3;
    h4= temH4;
    secrets=  quantosSecrets;
    itens = quantosItens;
    originX =  checkOriginX;
    originY=  checkOriginY;
    screenW =  checkScreenW;
    screenH =  checkScreenH;
    darkness=  checkDarkness;
    musicState= checkMusicState;
    BGM.restoreTrack(musicState);

    stage.loadObjects(tmxNumber+".json");
  }
}
