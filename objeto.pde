class Objeto {
  float x, y, v, g;
  float walkspeed;
  boolean caindo=true, walkanim=true, c=true;
  boolean bateuCabeca=false, colidiuEsq, colidiuDir;
  boolean sobre[] = new boolean [128*128];
  boolean aolado[] = new boolean [128*128];
  int xm, xM, ym, yM;
  int sizex, sizey, vm;
  int currentSprite;
  byte Jpower;
  void fall() {
    y=y+v;
    if (v<vm)v+=g;
  }
  void start() {
  }
  void jump() {
    caindo = true;
    SE.sJump();
    hero.caindoCounter+=50;
    //if (v==0) 
    v=-Jpower*0.1;
    if (hero.direita==true) poeira.cria(hero.x, hero.y-5);
    else poeira.cria(hero.x+hero.sizex, hero.y-5);
    y--;
  }
  void atualiza() {
  }
  void desenha() {
  }
  void maxmin() {
    xm= int((x+sizex*0.5)/tsize-2);
    ym= int((y-sizey*0.5)/tsize-2);
    xM= int((x+sizex*0.5)/tsize+2);
    yM= int((y-sizey*0.5)/tsize+2);
    if (xm<0)xm=0;
    if (xM>w)xM=w;
    if (ym<0)ym=0;
    if (yM>h)yM=h;
    if (y<0) yM=1;
  }
  void colide() {
    caindo = true;
    for (int i = 0; i < sobre.length; i++) {
      sobre[i]=false;
      aolado[i] = false;
    }
    walkanim=true;
    bateuCabeca=false;
    colidiuDir=false;
    colidiuEsq=false;
    for (int j =0;j<h;j++) {
      for (int i =0;i<w;i++) {

        //pés
        if (x>i*tsize-sizex && x< (i+1)*tsize && y >=j*tsize && y <=j*tsize+vm+1 && sprite[i+w*j]%tsw>sol && (caindo == false ||v>0)) {
          sobre[i+w*j]=true;
        }

        //cabeça
        if (x>i*tsize-sizex+walkspeed && x< (i+1)*tsize-walkspeed &&  y >=j*tsize+tsize+sizey-90*0.5 && y <=j*tsize+tsize+sizey && sprite[i+w*j]%tsw>sol && v<0) {
          sobre[i+w*j]=true;
        }    

        //esq //AQUI está o problema

        if (x>=i*tsize+tsize-4 && x<= i*tsize+tsize &&  y >j*tsize && y < j*tsize+tsize+sizey && sprite[i+w*j]%tsw>sol && (sobre[i+w*j]==false || caindo==true)) { //4
          x = i*tsize+tsize;
          //aolado[i+w*j]=true;
          walkanim=false;
          colidiuEsq=true;
        }

        //dir

        if (x>=i*tsize-sizex && x<= i*tsize-sizex+4 &&  y >j*tsize && y < j*tsize+tsize+sizey && sprite[i+w*j]%tsw>sol && (sobre[i+w*j]==false || caindo==true)) { //adicionei right e left
          x = i*tsize-sizex;
          //aolado[i+w*j]=true;
          walkanim=false;
          colidiuDir=true;
        }

        //pés2

        if (x>i*tsize-sizex+1 && x< (i+1)*tsize && y >=j*tsize && y <=j*tsize+vm+1 && sprite[i+w*j]%tsw>sol && aolado[i+w*j]==false) {
          caindo = false;
          v = 0;
          y = j*tsize;
        }

        //cabeça2

        if (x>i*tsize-sizex+walkspeed && x< (i+1)*tsize-walkspeed &&  y >=j*tsize+tsize+sizey-90*0.5+1 && y <=j*tsize+tsize+sizey && sprite[i+w*j]%tsw>sol /*&& aolado[i+w*j]==false*/ && v<0) {
          v = -v*0.1;
          y = j*tsize+tsize+sizey;
        }

        //sobre escada

        if (j>0 && x>i*tsize-sizex && x< (i+1)*tsize && y >=j*tsize && y <=j*tsize+vm+1 && sprite[i+w*j]==1&& sprite[i+w*(j-1)]!=1 && (v>=0)) {//(caindo == false ||v>0)){
          caindo = false;
          v = 0;
          y = j*tsize;
        }
      }
    }
  }
  boolean dentro() {
    boolean D = false;
    if (x<screenW && y<screenH) D= true;
    return (D);
  }
}
