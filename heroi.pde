class Heroi extends Objeto {

  boolean ativo=true;
  boolean direita=true;
  byte walk;
  byte walkT;
  int flinch;
  int life;
  int lifemax; //TODO: fazer isso funcionar
  boolean gb;
  boolean golpeCima=false;
  boolean golpeBaixo=false;
  boolean canDoubleJump=false;
  boolean dashRight=false;
  boolean dashLeft=false;
  int Jpower2;
  int caindoCounter;

  int xlimit, ylimit; //quão longe ele já chegou
  int pushing=0;

  void start() {
    direita=false;
    g=gNorm;
    v=0;
    ativo=true;
    vm=12;
    sizex=16;//16
    sizey=18;//22
    Jpower= 50;//80
    Jpower2 = 32;
    walkspeed=2;
    x=27*tsize-8;
    y=21*tsize;
    life=1;
    lifemax=1; 
    flinch=0;
    golpeCima=false;
    golpeBaixo=false;
    canDoubleJump=true;
    dashRight=false;
    dashLeft=false;
  }


  void desenha() {
    
    
//    if (direita==true) {
//      if (golpeBaixo==true) image (lesma[3+frames%2], x-8, y-sizey-9);
//      else if (caindo==true) image (lesma[2], x-8, y-sizey-9);
//      else {
//        if (dashRight==true && input.right==true && walkanim==true) image (lesma[0+(frames/2)%2], x-8, y-sizey-9);
//        else if (dashRight==false && input.right==true && walkanim==true) image (lesma[0+(frames/6)%2], x-8, y-sizey-9);
//        else if (input.right==true && walkanim==false && bumpT<=0) image (lesma[6+(frames/6)%2], x-8, y-sizey-9);
//        else image (lesma[0], x-8, y-sizey-9);          
//      }
//    }
//    else {
//      if (golpeBaixo==true) imageX (lesma[3+(frames/2)%2], x-8, y-sizey-9);
//      else if (caindo==true)imageX (lesma[2], x-8, y-sizey-9);
//      else {
//        if (dashLeft==true && input.left==true && walkanim==true) imageX (lesma[0+(frames/2)%2], x-8, y-sizey-9);
//        else if (dashLeft==false && input.left==true && walkanim==true) imageX (lesma[0+(frames/6)%2], x-8, y-sizey-9);
//        else if (input.left==true && walkanim==false && bumpT<=0) imageX (lesma[6+(frames/6)%2], x-8, y-sizey-9);
//        else imageX (lesma[0], x-8, y-sizey-9);
//      }
//    }
    if (pause==false){
    if (direita==true) {
      if (golpeBaixo==true) currentSprite= 3+frames%2;
      else if (caindo==true) currentSprite= 2;
      else {
        if (dashRight==true && input.right==true && walkanim==true) currentSprite= 0+(frames/2)%2;
        else if (dashRight==false && input.right==true && walkanim==true) currentSprite= 0+(frames/6)%2;
        else if (input.right==true && walkanim==false && bumpT<=0) currentSprite= 6+(frames/6)%2;
        else currentSprite= 0;          
      }
    }
    else {
      if (golpeBaixo==true) currentSprite= 3+(frames/2)%2;
      else if (caindo==true)currentSprite= 2;
      else {
        if (dashLeft==true && input.left==true && walkanim==true) currentSprite= 0+(frames/2)%2;
        else if (dashLeft==false && input.left==true && walkanim==true) currentSprite= 0+(frames/6)%2;
        else if (input.left==true && walkanim==false && bumpT<=0) currentSprite= 6+(frames/6)%2;
        else currentSprite= 0;
      }
    }

    if (walkanim==false) pushing++;
    else pushing=0;
    }
    
    if (direita==true) image (lesma[currentSprite], x-8, y-sizey-9);
    else imageX (lesma[currentSprite], x-8, y-sizey-9);
    
  }

void atualiza(){
  if (caindo==true) caindoCounter++;
  else caindoCounter=0;
  if (pause==false)move();
  if (caindo==true)fall();
  colide();
}

  void move() {
    if (flinch>0) {
      flinch--;
      if (v>=0 && ativo==false) {
        ativo=true;
      }
    }
    if (flinch>0 && v<0 && ativo==false) {
      if (direita==true)x--;
      else x++;
    }
    else {
      if (input.right == true && golpeBaixo==false && bumpT<=0) {
        x+=walkspeed;
        if (dashRight==true) x+=2;
        direita=true;
        walkT++;
        if (walkT>3) {
          walkT=0;
          walk++;
          if (walk>3) walk=0;
        }
        if (x>=originX+width-sizex && pushing>18) {
          crescedireita=true;
          screenvx=1;
          if (frames%5==0)SE.sPush();
        }
      }
      if (input.left == true && golpeBaixo==false && bumpT<=0) {
        x-=walkspeed;
        if (dashLeft==true) x-=2;
        direita=false;
        walkT++;
        if (walkT>3) {
          walkT=0;
          walk ++;
          if (walk>3) walk=0;
        }
        if (x<=originX && pushing>15) {
          crescedireita=false;
          screenvx=1;
          if (frames%5==0)SE.sPush();
        }
      }
    }
    if (caindo==false && (dashRight==true || dashLeft==true) && frames%4==0) SE.sSprint();
    if (caindo==false && dashRight==true && frames%6==0) {
      poeira.cria(hero.x, hero.y-5);
    }
    if (caindo==false && dashLeft==true && frames%6==0) poeira.cria(hero.x+hero.sizex, hero.y-5);
  }
  void fall() {
    caindoCounter++;
    super.fall();
  }
  void colide() {
    c = true; 
    gb= true;
    for (int i = 0; i < sobre.length; i++) {
      sobre[i]=false;
      aolado[i] = false;
    }
    walkanim=true;


    ///////////////////////////////////////////////////////////////////////////////////////  
    xm= int((x+sizex*0.5)/tsize);

    for (int j =ym;j<yM;j++) {
      //pés
      if (x>=xm*tsize-sizex -2 && x<= (xm+1)*tsize +1 && y >=j*tsize && y <=j*tsize+vm+1 && sprite[xm+w*j]%tsw>sol && (caindo == false ||v>0)) {
        sobre[xm+w*j]=true;
      }
      //pés2

      if (x>xm*tsize-sizex+1 && x< (xm+1)*tsize && y >=j*tsize && y <=j*tsize+vm+1 && sprite[xm+w*j]%tsw>sol && aolado[xm+w*j]==false) {
        c = false;
        v = 0;
        y = j*tsize;
      }

      //cabeça2

      if (x>xm*tsize-sizex+walkspeed && x< (xm+1)*tsize-walkspeed &&  y >=j*tsize+tsize+sizey-Jpower*0.1-1 && y <=j*tsize+tsize+sizey && sprite[xm+w*j]%tsw>sol) {//+1 é o que funcionava
        v = -v*0.1;
        y = j*tsize+tsize+sizey;
      }
    }
    maxmin();

    ///////////////////////////////////////////////////////////////////////////////////////



    for (int j =ym;j<yM;j++) {
      for (int i =xm;i<xM;i++) {
        //pés
        if (x>=i*tsize-sizex -2 && x<= (i+1)*tsize +1 && y >=j*tsize && y <=j*tsize+vm+1 && sprite[i+w*j]%tsw>sol && (caindo == false ||v>0)) {
          sobre[i+w*j]=true;
        }

        //esq //AQUI está o problema

        if (x>=i*tsize+tsize-4 && x<= i*tsize+tsize &&  y >j*tsize && y < j*tsize+tsize+sizey && sprite[i+w*j]%tsw>sol && (sobre[i+w*j]==false || caindo==true)) { //4 = perfeito
          //println (sobre[i+w*j]+","+ sobre[(i+1)+w*j]);
          if (sobre[(i+1)+w*j]==false) {
            x = i*tsize+tsize;
            //aolado[i+w*j]=true; 
            walkanim=false;
          }
        }

        //dir

        if (x>=i*tsize-sizex && x<= i*tsize-sizex+4 &&  y >j*tsize && y < j*tsize+tsize+sizey && sprite[i+w*j]%tsw>sol && (sobre[i+w*j]==false || caindo==true)) {
          x = i*tsize-sizex;
          //aolado[i+w*j]=true;
          walkanim=false;
        }

        //pés2

        if (x>i*tsize-sizex+1 && x< (i+1)*tsize && y >=j*tsize && y <=j*tsize+vm+1 && sprite[i+w*j]%tsw>sol && aolado[i+w*j]==false) {
          if (caindo==true) {
            //TODO: SE.LAND
            if (hero.direita==true) poeira.cria(hero.x, hero.y-5);
            else poeira.cria(hero.x+hero.sizex, hero.y-5);
          }
          if (golpeBaixo==true&&sprite[i+w*j]==15) {
            if (tmxNumber==2 && i<9) sprite[i+w*j]=48;
            else sprite[i+w*j]=0;
            if (brickB[0].anim>=6)brickB[0].cria(i*tsize,j*tsize);
            else brickB[1].cria(i*tsize,j*tsize);
          }
          if (golpeBaixo==true) bump2();
          c = false;
          v = 0;
          y = j*tsize;
          gb=false;
        }

        //cabeça2

        if (x>i*tsize-sizex+walkspeed && x< (i+1)*tsize-walkspeed &&  y >=j*tsize+tsize+sizey-Jpower*0.1+1 && y <=j*tsize+tsize+sizey && sprite[i+w*j]%tsw>sol /*&& aolado[i+w*j]==false*/ && v<0) {//coloquei +1  pra não dar o bug de subir a escada no ponto mínimo
          v = -v*0.1;
          y = j*tsize+tsize+sizey;
        }
      }
    }
    caindo=c;
    if (gb==false)golpeBaixo=false;

    if (x>originX+width-sizex) {
      x=originX+width-sizex;
      walkanim=false;
    }
    if (x<originX) {
      x=originX;
      walkanim=false;
    }
    if (y>=originY+height) {
      y=originY+height;
      caindo=false;
      v=0;
      if (golpeBaixo==true) {
        golpeBaixo=false;
        crescebaixo=true;
        screenvy=1.6;
        bump2();
      }
    }
    if (y<originY+sizey) {
      y=originY+sizey;
      if (v<0) v = -v*0.1;
      if (golpeCima==true) {
        crescebaixo=false;
        screenvy=1; 
        bump();
      }
    }
    if (v>=0)golpeCima=false;
    if (caindo==false) canDoubleJump=true;

    if (walkanim==false) {
      dashLeft=false;
      dashRight=false;
    }
  }

  void levaDano(float X) {
    SE.sDamage();
    life-=int(X);
    flinch=40;
    caindo=true;
    v=-3;
    ativo=false;
    golpeBaixo=false;
    dashRight=false;
    dashLeft=false;
    if (life<1) gameOver();
  }
}
