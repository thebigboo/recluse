class Input {
  boolean A;
  boolean left;
  boolean right;
  boolean up;
  boolean down;
  boolean start;
  boolean key1;
  boolean key2;
  boolean key3;
}

void keyPressed() {
  if (key == CODED) {

    switch(keyCode) {
    case UP:
      if (input.up==false) {
        if (state==3 && pause==true){
          menu--;
          if (menu<0) menu=2;
        }
        if (state==3 && bumpT<=0 && pause==false) {
          if (hero.caindoCounter<3 || hero.y>=originY+height-1) {
            hero.jump();
            hero.canDoubleJump=true;
          }
          else if (hero.canDoubleJump==true && itemPuloDuplo==true && hero.v>-hero.Jpower2*0.1) {
            hero.caindo = true;
            SE.sJump();
            hero.v=-hero.Jpower2*0.1;
            hero.canDoubleJump=false;
            hero.golpeCima=false;
            if (hero.direita==true) poeira.cria(hero.x, hero.y-5);
            else poeira.cria(hero.x+hero.sizex, hero.y-5);
          }
        }
      }
      input.up=true;
      break;
    case DOWN:
      input.down=true;
        if (state==3 && pause==true){
          menu++;
          if (menu>2) menu=0;
        }
      if (pause==false && hero.caindo==true && hero.golpeBaixo==false && itemGolpeBaixo==true && bumpT<=0 && hero.y<originY+height-2) {
        hero.golpeBaixo=true;
        hero.dashRight=false;
        hero.dashRight=false;
        hero.v=hero.vm*0.75;
      }
      break;
    case LEFT:
      input.left=true;
      if (pause==false){
      if (hero.ativo==true && bumpT<=0)hero.direita=false;
      if (doubleTapL>0 && itemDash==true && hero.caindo==false && hero.golpeBaixo==false) {
        hero.dashLeft=true;
      }
      hero.dashRight=false;
      }
      break;
    case RIGHT:
      input.right=true;
      if (pause==false){
      if (hero.ativo==true && bumpT<=0)hero.direita=true;
      if (doubleTapR>0 && itemDash==true && hero.caindo==false && hero.golpeBaixo==false) {
        hero.dashRight=true;
      }
      hero.dashLeft=false;
      }
      break;
    }
  }
  if (keyCode==ESC) {
    key=ENTER;
  }
  if (keyCode==ENTER) {
    if (state==4 && timer*60+frames>=900){
      backToTitle();
    }
    if (input.start==false && state==3 && canPause==true && showText==false) {
      if (pause==false){
        pause=true;
        menu=0;
      }
      else if (pause==true){
        if (menu==0) pause=false;
        else if (menu==1){
          backToTitle();
        }
        else if (menu==2){
          exit();
        }
      }
    }
    input.start=true;
  }
  if (key == 'a' || key == 'A') {
    input.key1=true;
  }
  if (key == 'n' || key == 'N') {
    input.key2=true;
  }
  if (key == 'y' || key == 'Y') {
    input.key3=true;
  }
//  if (key == 'm') {
//    println(hero.x+", "+hero.y);
//  }
  if (key == 'p') {
    originX=2.92;
    originY=8.2;
    hero.x=mouseX;
    hero.y=mouseY;
    screenW=971;
    screenH=593;
  }
  if (key == 'Z' || key == 'z' || key == ' ') {
    if (input.A==false) {

      if (pause==false && state==3 && bumpT<=0) {
        if (hero.caindoCounter<3 || hero.y>=originY+height-1) {
          hero.jump();
          hero.canDoubleJump=true;
          if (freeze==true && showText==true && currentItem==2 && (hero.dashRight==true || hero.dashLeft==true)) {
            freeze=false;
            showText=false;
          }
        }
        else if (hero.canDoubleJump==true && itemPuloDuplo==true && hero.v>-hero.Jpower2*0.1) {
          hero.caindo = true;
          SE.sJump();
          hero.v=-hero.Jpower2*0.1;
          hero.canDoubleJump=false;
          hero.golpeCima=false;
          if (hero.direita==true) poeira.cria(hero.x, hero.y-5);
          else poeira.cria(hero.x+hero.sizex, hero.y-5);
          if (freeze==true && showText==true && currentItem==1) {
            freeze=false;
            showText=false;
          }
        }
      }
    }
    input.A=true;
  }
}

void keyReleased() {
  if (key == CODED) {
    switch(keyCode) {
    case UP:
      input.up=false;
      break;
    case DOWN:
      input.down=false;
      break;
    case LEFT:
      input.left=false;
      doubleTapL=8;
      hero.dashLeft=false;
      break;
    case RIGHT:
      input.right=false;
      doubleTapR=8;
      hero.dashRight=false;
      break;
    case ENTER:
      input.start=false;
      break;
    }
  }
  if (keyCode==ENTER) {
    input.start=false;
  }
  if (key == 'Z' || key == 'z' || key == ' ') {
    input.A=false;
  }
    if (key == 'a' || key == 'A') {
    input.key1=false;
  }
  if (key == 'n' || key == 'N') {
    input.key2=false;
  }
  if (key == 'y' || key == 'Y') {
    input.key3=false;
  }
}
